//Variables principales
var primerValor = 0;
var operador = "";
var segundoValor = "";
var resultado = 0;
var porTeclado = false;


function agregarBoton(id){
    var boton = document.getElementById(id);
    var valor = boton.id
    var texto = document.getElementById("campoTexto");
    texto.value = texto.value + valor;
}

function raiz(){
    var numero = document.getElementById("campoTexto");
    var resultado = Math.sqrt(numero.value)
    numero.value = resultado;
}

function entreUno(){
    var numero = document.getElementById("campoTexto");
    var resultado = 1 / numero.value
    numero.value = resultado;
}

function resetearVariables(){
    primerValor = 0;
    operador = "";
    segundoValor = "";
    resultado = 0;
    porTeclado = false;
}

function borrarTodo(){
    resetearVariables()
    document.getElementById("campoTexto").value = "";
}

function retroceder(){
    var texto = document.getElementById("campoTexto").value;

    if(texto.length <= 1){
        document.getElementById("campoTexto").value = "";
        console.log("hola")

    }else{   
        console.log("adios")
        var nuevaCadena = ""
        console.log(texto.length-1)
        for (var index = 0; index < texto.length-1; index++) {
            nuevaCadena += texto[index];
            
        }
        document.getElementById("campoTexto").value = nuevaCadena;
    }

}

function sumar(){
    var numero = document.getElementById("campoTexto");
    primerValor = numero.value;
    operador = "+";
    if(!porTeclado){
        numero.value = numero.value + operador;
    }
 
}

function restar(){
    var numero = document.getElementById("campoTexto");
    primerValor = numero.value;
    operador = "-";
    if(!porTeclado){
        numero.value = numero.value + operador;
    } 
}

function dividir(){
    var numero = document.getElementById("campoTexto");
    primerValor = numero.value;
    operador = "/";
    if(!porTeclado){
        numero.value = numero.value + operador;
    } 
}

function multiplicar(){
    var numero = document.getElementById("campoTexto");
    primerValor = numero.value;
    operador = "*";
    if(!porTeclado){
        numero.value = numero.value + operador;
    } 
}

function porcentaje(){
    var numero = document.getElementById("campoTexto");
    primerValor = numero.value;
    operador = "%";
    if(!porTeclado){
        numero.value = numero.value + operador;
    } 
}

function calcular(){
var segundoValor = obtenerSegundoValor();
switch (operador) {
    case "+":
        resultado = parseFloat(primerValor)+parseFloat(segundoValor);
        document.getElementById("campoTexto").value = resultado
        resetearVariables()
        break;
    case "-":
        resultado = parseFloat(primerValor)-parseFloat(segundoValor);
        document.getElementById("campoTexto").value = resultado
        resetearVariables()

        break;
    case "*":
        resultado = parseFloat(primerValor)*parseFloat(segundoValor);
        document.getElementById("campoTexto").value = resultado
        resetearVariables()

        break;
    case "/":
        resultado = parseFloat(primerValor)/parseFloat(segundoValor);
        document.getElementById("campoTexto").value = resultado
        resetearVariables()

        break;
    case "%":
        resultado = parseFloat(primerValor)%parseFloat(segundoValor);
        document.getElementById("campoTexto").value = resultado
        resetearVariables()

        break;

    default:
        break;
}
}

function obtenerSegundoValor(){
    
    var texto = document.getElementById("campoTexto").value
    if(texto.indexOf(operador)){
        for (var index = texto.indexOf(operador)+1; index < texto.length; index++) {
        
            segundoValor = segundoValor + texto[index];
        }
        return segundoValor;
    }
   

    
    
}

function botones (valor) {
    var codigo = event.which || event.keyCode;

    console.log("Presionada: " + codigo);
    porTeclado = true; 
    switch (codigo) {
        case 187:
            sumar();
            break;
        case 109:
            restar();
            break;
        case 106:
            multiplicar();
            break;
        case 111:
            dividir();
            break;
        case 53:
            porcentaje();
            break;
        case 48:
            calcular();
            break;
    
        default:
            break;
    }
    

    
}

function ultimaEntrada(){
    var texto = document.getElementById("campoTexto").value

    if(!texto.indexOf(operador)){
        borrarTodo();
    }else{
        var nuevaCadena = "";
        for (var index = 0; index < texto.indexOf(operador)+1; index++) {       
            nuevaCadena = nuevaCadena + texto[index];
        }
        document.getElementById("campoTexto").value = nuevaCadena
    }

}

function antesOperador(){
    var texto = document.getElementById("campoTexto").value

    var textoAntesOperador = "";

    for (var index = 0; index < texto.indexOf(operador)+1; index++) {
        textoAntesOperador = textoAntesOperador + texto[index];

    }

    return textoAntesOperador;
}

function despuesOperador(){
    var texto = document.getElementById("campoTexto").value

    var textoAntesOperador = antesOperador();
    var textoDespuesOperador = "";

    for (var index = textoAntesOperador.length; index < texto.length; index++) {
        textoDespuesOperador += texto[index];               
    }
    return textoDespuesOperador;
}

function cambiarSigno(){
    var texto = document.getElementById("campoTexto").value
    var nuevaCadena = ""
    if(!texto.indexOf(operador)){

            for (var index = 0; index < texto.length; index++) {       
                if(texto[0] == "-"){                                       
                    document.getElementById("campoTexto").value = texto.replace('-','')
                }else{
                   nuevaCadena = "-" + texto[index];
                   document.getElementById("campoTexto").value = nuevaCadena
                }
            
            }

    }else{
        var textoAntesOperador = antesOperador();        
        var textoDespuesOperador = despuesOperador();
        
            if(texto[textoAntesOperador.length] == "-"){ 
                document.getElementById("campoTexto").value = texto.replace('-','')
                nuevaCadena = document.getElementById("campoTexto").value;
            }else{
               nuevaCadena = textoAntesOperador+"-"+textoDespuesOperador;
               document.getElementById("campoTexto").value = nuevaCadena

            }
    }

    }
